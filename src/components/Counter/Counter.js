import React, { Component } from 'react';

class Counter extends Component {


    render() {

        return (
            <div >
                <button onClick={this.props.clicked}>{this.props.children}</button>
            </div>
        )
    }
}

export default Counter