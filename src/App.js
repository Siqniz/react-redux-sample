import React, { Component } from 'react';
import CounterView from './containers/Counter/CounterView';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <CounterView />
      </div>
    );
  }
}

export default App;
