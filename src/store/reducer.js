const initialState = {
    counter: 1,
    results: []
}


const reducer = (state = initialState, action) => {
    if (action.type === "INCREMENT") {
        return {
            ...state,
            counter: state.counter + action.value
        }
    }
    if (action.type === 'DECREMENT') {
        return {
            ...state,
            counter: state.counter - action.value
        }
    }
    if (action.type === 'STORE_RESULT') {
        return {
            ...state,
            results: state.results.concat({ id: new Date().getTime(), value: state.counter })
        }
    }
    if (action.type === 'DELETE_RESULT') {
        const updatedArray = state.results.filter(result => result.id !== action.resultEleId)
        return {
            ...state,
            results: updatedArray
        }
    }

    return state;
}

export default reducer