import React, { Component } from 'react';
import { connect } from 'react-redux'

import Counter from '../../components/Counter/Counter'


class CounterView extends Component {

    render() {
        const mainstyle = { width: '60%', margin: 'auto' }
        const innerstyle = { backgroundColor: 'lightGreen', margin: '5px', padding: '5px' }
        const twinpanel = { width: '45%', display: 'inline-block' };
        return (
            <div style={mainstyle}>
                <div>
                    <div style={innerstyle}>Answer: {this.props.ctr}</div>
                    <div style={twinpanel}>
                        INCREMENT: <Counter clicked={this.props.onIncrementCounter} >+</Counter>
                    </div>
                    <div style={twinpanel}>
                        DECREMENT: <Counter clicked={this.props.onDecrementCounter} >-</Counter>
                    </div>
                    <hr />
                    <button onClick={this.props.onStoreResult}>Store Result</button>
                    <ul>
                        {this.props.storedResults.map(strResult => {
                            return (<li key={strResult.id} onClick={() => this.props.ondDeleteResult(strResult.id)}>{strResult.value}</li>)
                        })}

                    </ul>
                </div>
                {/* <div style={innerstyle}>
                    Sum: {this.props.value} 
                </div> */}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        ctr: state.counter,
        storedResults: state.results
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onIncrementCounter: () => dispatch({ type: 'INCREMENT', value: 2 }),
        onDecrementCounter: () => dispatch({ type: 'DECREMENT', value: 1 }),
        onStoreResult: () => dispatch({ type: 'STORE_RESULT' }),
        ondDeleteResult: (id) => dispatch({ type: 'DELETE_RESULT', resultEleId:id }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CounterView); 